package sample.controller;

import org.apache.commons.compress.utils.IOUtils;
import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.ui.Model;
import sample.model.*;
import sample.repo.*;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.AllOf.allOf;
import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by vincenzo on 04/09/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment =  SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ModelControllerTest {
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;
    @Autowired
    protected BinaryTrainRepository binaryRepository;
    @Autowired
    protected GridPolylineRepository gridPolylineRepository;
    @Autowired
    protected LineRepository lineRepository;
    @Autowired
    protected PointRepository pointRepository;
    @Autowired
    protected PrototypeRepository prototypeRepository;
    @Autowired
    protected RailRepository railRepository;
    @Autowired
    protected RailwaySleeperRepository railwaySleeperRepository;
    @Autowired
    protected SupportARepository supportARepository;
    @Autowired
    protected InventaryBinaryRepository inventaryBinaryRepository;
    @Autowired
    protected SupportBRepository supportBRepository;
    @Autowired
    protected SupportLRepository supportLRepository;

    Prototype p;
    ArrayList<BinaryTrain> binaryTrains = new ArrayList<>();
    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .defaultRequest(get("/").with(user("vincemess").password("password").roles("super user")))
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
        p = new Prototype("nome prototipo");
        p.setNrModel("A342354676");
        p.setNrArticle("542432543");
        p.setClient("ferrovia");


        ArrayList<Point> vertex =  new ArrayList<>();
        vertex.add(new Point(0,0));
        vertex.add(new Point(0,1));
        vertex.add(new Point(1,1));
        vertex.add(new Point(1,0));
        GridPolyline grid = new GridPolyline("grid_center",vertex,new Point(0,0),new Point(4,4),new Point(1,1));
        grid.setNumber(2);
        grid.setPath("");
        grid.setTranslationGridFromCenter(new Point(3,3));

        vertex =  new ArrayList<>();
        vertex.add(new Point(0,0));
        vertex.add(new Point(0,1));
        vertex.add(new Point(1,1));
        vertex.add(new Point(1,0));
        GridPolyline grid_sx = new GridPolyline("grid_sx",vertex,new Point(0,0),new Point(4,4),new Point(1,1));
        grid.setNumber(1);
        grid.setPath("");
        grid.setTranslationGridFromCenter(new Point(3,3));

        vertex = new ArrayList<>();
        vertex.add(new Point(0,0));
        vertex.add(new Point(0,1));
        vertex.add(new Point(1,1));
        vertex.add(new Point(1,0));
        GridPolyline grid_dx = new GridPolyline("grid_dx",vertex,new Point(0,0),new Point(4,4),new Point(1,1));
        grid.setNumber(3);
        grid.setPath("");
        grid.setTranslationGridFromCenter(new Point(3,3));

        GridPolyline[] grids = new GridPolyline[3];
        grids[0] = grid_sx;
        grids[1] = grid;
        grids[2] = grid_dx;

        SupportA supportA = new SupportA("supportA",new Point(1,1));
        supportA.setTranslationSupportAFromCenter(new Point(3,3));
        supportA.setPath("supportA");
        SupportA[] supportAS = new SupportA[4];
        supportAS[0] = supportA;
        supportAS[1] = new SupportA(supportA);
        supportAS[2] = new SupportA(supportA);
        supportAS[3] = new SupportA(supportA);
        SupportB supportB = new SupportB("supportB",new Point(3,3));
        supportB.setTranslationSupportBFromCenter(new Point(4,4));
        supportB.setPath("path");
        SupportB[] supportBs = new SupportB[8];
        supportBs[0] = supportB;
        supportBs[1] = new SupportB(supportB);
        supportBs[2] = new SupportB(supportB);
        supportBs[3] = new SupportB(supportB);
        supportBs[4] = new SupportB(supportB);
        supportBs[5] = new SupportB(supportB);
        supportBs[6] = new SupportB(supportB);
        supportBs[7] = new SupportB(supportB);
        SupportL supportL = new SupportL("supportL",new Point(3,4));
        supportL.setTranslationSupportLFromCenter(new Point(2,3));
        supportL.setPath("path");
        SupportL[] supportLs = new SupportL[4];
        supportLs[0] = supportL;
        supportLs[1] = supportL;
        supportLs[2] = supportL;
        supportLs[3] = supportL;
        Line linesSX = new Line(new Point(1,1),new Point(2,1),1);
        Line linesDX = new Line(new Point(2,2),new Point(1,2),1);
        String name = "rail";
        Rail rail = new Rail(name,linesSX,linesDX,new Point(4,5));
        Point[] translations = new Point[2];
        translations[0] = new Point(-1,0);
        translations[1] = new Point(1,0);
        rail.setTranslationRailFromCenter(translations);


        RailwaySleeper sleeper = new RailwaySleeper(name,new Point(5,6));
        sleeper.setTranslationBeetweenSleepers(new Point(3,4));
        sleeper.setTranslationSleepersFromCenter(new Point(2,2));
        BinaryTrain binaryTrain = new BinaryTrain("nameBinary",rail,grids,sleeper,supportAS,supportBs,supportLs);
        binaryTrain.setTranslationFromOtherBinary(new Point(6,5));
        binaryTrains.add(binaryTrain);
        p.setBinaries(binaryTrains);
        //p = savePrototype(p);



    }
   @Ignore
    @Test
    public void greetingRoot() throws Exception {
        Matcher<String> matcher = allOf(containsString("name"));
        this.mockMvc.perform(get("/prototype/add")).andExpect(status().isOk()).andExpect(content().string(matcher));


    }


    @Ignore
    @Test
    public void addPrototype() throws Exception {
        Path currentRelativePath = Paths.get("");
        String path = currentRelativePath.toAbsolutePath().toString();
        path = path + "/src/test/resources/singolo_binario_prova.dxf";
        byte[] data = Files.readAllBytes(Paths.get(path));
        MockMultipartFile firstFile = new MockMultipartFile("file", "" +
                "singolo_binario_prova.dxf", "text",data);
        this.mockMvc.perform(MockMvcRequestBuilders.post("/prototype/add").param("layerR","rotaie").param("layerG","griglie").param("name", "p")).andExpect(status().isOk());

    }
    @Ignore
    @Test
    public void createPrototype() throws Exception {
        String[] nameBinaries = new String[1];
        nameBinaries[0] = "Binary45";
        String[] numberSupportA = new String[1];
        numberSupportA[0] = "4";
        this.mockMvc.perform(post("/prototype/create/{id_prototype}",p.getId()).param("nameBinary",nameBinaries).param("supportAnumber",numberSupportA)).andExpect(status().isOk());

    }


}