package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 03/09/17.
 */
public class SupportLTest {
    Point center = new Point(0,0);
    String name = "supportL";
    String path = "/path/image/support";
    SupportL supportL = new SupportL(name,center);
    Point translation = new Point(0,0);
    @Before
    public void setUp(){
        supportL.setTranslationSupportLFromCenter(translation);
        supportL.setPath(path);
    }

    @Test
    public void getPath() throws Exception {
        Assert.assertEquals(path,supportL.getPath());
    }

    @Test
    public void getName() throws Exception {
        Assert.assertEquals(name,supportL.getName());
    }

    @Test
    public void getCenter() throws Exception {
        Assert.assertEquals(center,supportL.getCenter());
    }
    @Test
    public void getTranslationSupportLFromCenter() throws Exception {
    }

}