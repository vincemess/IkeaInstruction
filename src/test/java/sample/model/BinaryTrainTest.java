package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 04/09/17.
 */
public class BinaryTrainTest {
    String name = "supportB";
    String nameBinary = "binary";
    String path = "/path/image/support";
    BinaryTrain binaryTrain;
    SupportA[] supportAs = new SupportA[4];
    SupportB[] supportBs = new SupportB[8];
    SupportL[] supportLs = new SupportL[4];
    GridPolyline[] grids = new GridPolyline[3];
    Rail rail ;
    RailwaySleeper sleeper;
    Point translation;
    Point center;
    @Before
    public void setUp(){
        center = new Point(0,0);
        Point max = new Point(10,10);
        Point min = new Point(1,1);
        ArrayList<Point> vertex =  new ArrayList<>();

        translation = new Point(0,0);
        int number = 2;
        vertex.add(new Point(0,0));
        vertex.add(new Point(0,1));
        vertex.add(new Point(1,1));
        vertex.add(new Point(1,0));
        GridPolyline grid = new GridPolyline(name,vertex,center,max,min);
        grid.setNumber(number);
        grid.setPath(path);
        grid.setTranslationGridFromCenter(translation);
        grids[0] = grid;
        grids[1] = grid;
        grids[2] = grid;
        SupportA supportA = new SupportA(name,center);
        supportA.setTranslationSupportAFromCenter(translation);
        supportA.setPath(path);
        supportAs[0] = supportA;
        supportAs[1] = supportA;
        supportAs[2] = supportA;
        supportAs[3] = supportA;
        SupportB supportB = new SupportB(name,center);
        supportB.setTranslationSupportBFromCenter(translation);
        supportB.setPath(path);
        supportBs[0] = supportB;
        supportBs[1] = supportB;
        supportBs[2] = supportB;
        supportBs[3] = supportB;
        supportBs[4] = supportB;
        supportBs[5] = supportB;
        supportBs[6] = supportB;
        supportBs[7] = supportB;
        SupportL supportL = new SupportL(name,center);
        supportL.setTranslationSupportLFromCenter(translation);
        supportL.setPath(path);
        supportLs[0] = supportL;
        supportLs[1] = supportL;
        supportLs[2] = supportL;
        supportLs[3] = supportL;
        Point start = new Point(1,1);
        Point end = new Point(2,1);
        Line linesSX = new Line(start,end,1);
        Line linesDX = new Line(start,end,1);
        String name = "rail";
        rail = new Rail(name,linesSX,linesDX,center);
        Point[] translations = new Point[2];
        translations[0] = new Point(-1,0);
        translations[1] = new Point(1,0);
        rail.setTranslationRailFromCenter(translations);


        sleeper = new RailwaySleeper(name,center);
        Point translation = new Point(0,0);
        sleeper.setTranslationBeetweenSleepers(translation);
        sleeper.setTranslationSleepersFromCenter(translation);
        binaryTrain = new BinaryTrain(nameBinary,rail,grids,sleeper,supportAs,supportBs,supportLs);
        binaryTrain.setTranslationFromOtherBinary(translation);


    }
    @Test
    public void getSupportAList() throws Exception {
        Assert.assertArrayEquals(supportAs,binaryTrain.getSupportAList());
    }

    @Test
    public void getTranslationFromOtherBinary() throws Exception {
        Assert.assertEquals(translation,binaryTrain.getTranslationFromOtherBinary());
    }

    @Test
    public void getCamera() throws Exception {
        Assert.assertEquals("binary_camera",binaryTrain.getCamera());

    }

    @Test
    public void getName() throws Exception {
        Assert.assertEquals(nameBinary,binaryTrain.getName());
    }

    @Test
    public void getRail() throws Exception {
        Assert.assertEquals(rail,binaryTrain.getRail());

    }

    @Test
    public void getGrids() throws Exception {
        Assert.assertArrayEquals(grids,binaryTrain.getGrids());
    }

    @Test
    public void getSleeper() throws Exception {
        Assert.assertEquals(sleeper,binaryTrain.getSleeper());
    }

    @Test
    public void getCenter() throws Exception {
        Assert.assertEquals(center,binaryTrain.getCenter());
    }

    @Test
    public void getDistanceBetweenSleepers() throws Exception {
        binaryTrain.setDistanceBetweenSleepers(0.6);
        Assert.assertEquals(0.6,binaryTrain.getDistanceBetweenSleepers(),0);
    }

    @Test
    public void getSupportBList() throws Exception {
        Assert.assertArrayEquals(supportBs,binaryTrain.getSupportBList());
    }



    @Test
    public void getSupportLList() throws Exception {
        Assert.assertArrayEquals(supportLs,binaryTrain.getSupportLList());
    }

}