package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 03/09/17.
 */
public class PointTest {
    Point point = new Point(0,0);
    @Before
    public void setUp(){

    }

    @Test
    public void getX() throws Exception {
        Assert.assertEquals(point.getX(),0,0);
        point.setX(-1);
        Assert.assertEquals(point.getX(),-1,0);
        point.setX(1.5);
        Assert.assertEquals(point.getX(),1.5,0);
    }
    @Test
    public void getY() throws Exception {
        Assert.assertEquals(point.getY(),0,0);
        point.setY(-1);
        Assert.assertEquals(point.getY(),-1,0);
        point.setY(1.5);
        Assert.assertEquals(point.getY(),1.5,0);
    }


}