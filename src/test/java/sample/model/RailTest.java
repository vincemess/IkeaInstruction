package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 03/09/17.
 */
public class RailTest {
    Point start = new Point(1,1);
    Point end = new Point(2,1);
    Line linesSX = new Line(start,end,1);
    Line linesDX = new Line(start,end,1);
    String name = "rail";
    Point center = new Point(0,0);
    Rail rail = new Rail(name,linesSX,linesDX,center);
    double[] angle = new double[2];
    Point[] translation = new Point[2];
    @Before
    public void setUp(){
        angle[0] = Math.PI/2;
        angle[1] = Math.PI;
        rail.setAngle(angle);

        translation[0] = start;
        translation[1] = end;
        rail.setTranslationRailFromCenter(translation);
    }
    @Test
    public void getAngle() throws Exception {
        Assert.assertEquals(angle,rail.getAngle());
    }

    @Test
    public void getTranslationRailFromCenter() throws Exception {
       assertArrayEquals(rail.getTranslationRailFromCenter(),translation);
    }

    @Test
    public void getLinesSX() throws Exception {
        Assert.assertEquals(linesSX,rail.getLinesSX());
    }

    @Test
    public void getName() throws Exception {
        Assert.assertEquals(name,rail.getName());
    }

    @Test
    public void getLinesDX() throws Exception {
        Assert.assertEquals(linesDX,rail.getLinesDX());
    }

    @Test
    public void getCenter() throws Exception {
        Assert.assertEquals(center,rail.getCenter());
    }

}