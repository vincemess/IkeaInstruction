package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 04/09/17.
 */
public class InventaryBinaryTest {

    InventaryBinary inventaryBinary;
    int[] numbers = new int[3];
    String name = "Binary";
    int numberSupportA = 4;

    @Before
    public void setUp(){
        numbers[0] = 1;
        numbers[1] = 2;
        numbers[2] = 3;
        inventaryBinary = new InventaryBinary(name,numbers,numberSupportA);

    }


    @Test
    public void getNumberSupportB() throws Exception {
        Assert.assertEquals(numberSupportA*InventaryBinary.NUMBEROFSUPPORTBNECESSARY,inventaryBinary.getNumberSupportB());
    }

    @Test
    public void getNumberSupportL() throws Exception {
        Assert.assertEquals(numberSupportA*InventaryBinary.NUMBEROFSUPPORTLNECESSARY,inventaryBinary.getNumberSupportL());
    }

    @Test
    public void getNameBinary() throws Exception {
        Assert.assertEquals(name,inventaryBinary.getNameBinary());
    }

    @Test
    public void getGridsNumber() throws Exception {
        Assert.assertArrayEquals(numbers,inventaryBinary.getGridsNumber());
    }

    @Test
    public void getNumberSupportA() throws Exception {
        Assert.assertEquals(numberSupportA,inventaryBinary.getNumberSupportA());
    }

}