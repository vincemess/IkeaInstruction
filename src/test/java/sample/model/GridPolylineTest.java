package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 04/09/17.
 */
public class GridPolylineTest {
    Point center = new Point(0,0);
    Point max = new Point(10,10);
    Point min = new Point(1,1);
    ArrayList<Point> vertex =  new ArrayList<>();
    String name = "supportB";
    String path = "/path/image/support";
    GridPolyline grid = new GridPolyline();
    Point translation = new Point(0,0);
    List<Point> objectCoordinates;
    int number = 2;
    @Before
    public void setUp(){


        vertex.add(new Point(0,0));
        vertex.add(new Point(0,1));
        vertex.add(new Point(1,1));
        vertex.add(new Point(1,0));
        grid = new GridPolyline(name,vertex,center,max,min);
        grid.setNumber(number);
        grid.setPath(path);
        objectCoordinates = grid.getObjectCoordinates();
        grid.setTranslationGridFromCenter(translation);

    }
    @Test
    public void getPath() throws Exception {
        Assert.assertEquals(path,grid.getPath());
    }

    @Test
    public void getMax() throws Exception {
        Assert.assertEquals(max,grid.getMax());
    }

    @Test
    public void getMin() throws Exception {
        Assert.assertEquals(min,grid.getMin());
    }

    @Test
    public void getNumber() throws Exception {
        Assert.assertEquals(number,grid.getNumber());
    }

    @Test
    public void getPoints() throws Exception {
        Assert.assertEquals(vertex,grid.getPoints());
    }

    @Test
    public void getCenter() throws Exception {
        Assert.assertEquals(center,grid.getCenter());
    }

    @Test
    public void getObjectCoordinates() throws Exception {
        Assert.assertEquals(objectCoordinates,grid.getObjectCoordinates());
    }

    @Test
    public void getName() throws Exception {
        Assert.assertEquals(name,grid.getName());
    }

    @Test
    public void getTranslationGridFromCenter() throws Exception {
        Assert.assertEquals(translation,grid.getTranslationGridFromCenter());
    }

}