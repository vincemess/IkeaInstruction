package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 03/09/17.
 */
public class LineTest {
    Point start = new Point(0,0);
    Point end = new Point(0,1);
    Point center = new Point(0,0.5);
    Line line = new Line(start,end, 1);

    @Before
    public void setUp(){
        line.setCenter(center);
    }

    @Test
    public void getAngle() throws Exception {
        line.setAngle(0.5);
        Assert.assertEquals(line.getAngle(), 0.5,0);
    }

    @Test
    public void getCenter() throws Exception {
        Assert.assertEquals(center, line.getCenter());
    }

    @Test
    public void getLenght() throws Exception {
    Assert.assertEquals(line.getLenght(),1,0);
    }

    @Test
    public void getStart() throws Exception {
        Assert.assertEquals(start,line.getStart());
    }

    @Test
    public void getEnd() throws Exception {
        Assert.assertEquals(end,line.getEnd());
    }

}