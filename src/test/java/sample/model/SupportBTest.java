package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 03/09/17.
 */
public class SupportBTest {
    Point center = new Point(0,0);
    String name = "supportB";
    String path = "/path/image/support";
    SupportB supportB = new SupportB(name,center);
    Point translation = new Point(0,0);
    @Before
    public void setUp(){
        supportB.setTranslationSupportBFromCenter(translation);
        supportB.setPath(path);
    }

    @Test
    public void getPath() throws Exception {
        Assert.assertEquals(path,supportB.getPath());
    }

    @Test
    public void getName() throws Exception {
        Assert.assertEquals(name,supportB.getName());
    }

    @Test
    public void getCenter() throws Exception {
        Assert.assertEquals(center,supportB.getCenter());
    }




    @Test
    public void getTranslationSupportBFromCenter() throws Exception {
        Assert.assertEquals(translation,supportB.getTranslationSupportBFromCenter());
    }

}