package sample.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 03/09/17.
 */
public class SupportATest {
    Point center = new Point(0,0);
    String name = "supportA";
    String path = "/path/image/support";
    SupportA supportA = new SupportA(name,center);
    Point translation = new Point(0,0);
    @Before
    public void setUp(){
        supportA.setTranslationSupportAFromCenter(translation);
        supportA.setPath(path);
    }

    @Test
    public void getPath() throws Exception {
        Assert.assertEquals(path,supportA.getPath());
    }

    @Test
    public void getName() throws Exception {
        Assert.assertEquals(name,supportA.getName());
    }

    @Test
    public void getCenter() throws Exception {
        Assert.assertEquals(center,supportA.getCenter());
    }

    @Test
    public void getTranslationSupportAFromCenter() throws Exception {
        Assert.assertEquals(translation,supportA.getTranslationSupportAFromCenter());
    }

}