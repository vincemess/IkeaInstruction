package sample.model;

import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vincenzo on 04/09/17.
 */
public class RailwaySleeperTest {
    Point center = new Point(0,0);
    String name = "sleeper";
    RailwaySleeper sleeper = new RailwaySleeper(name,center);
    Point translation = new Point(0,0);
    @Before
    public void setUp(){
        sleeper.setTranslationBeetweenSleepers(translation);
        sleeper.setTranslationSleepersFromCenter(translation);
    }
    @Test
    public void getTranslationBeetweenSleepers() throws Exception {
        Assert.assertEquals(translation,sleeper.getTranslationBeetweenSleepers());
    }

    @Test
    public void getName() throws Exception {
        Assert.assertEquals(name, sleeper.getName());
    }

    @Test
    public void getTranslationSleepersFromCenter() throws Exception {
        Assert.assertEquals(translation,sleeper.getTranslationSleepersFromCenter());
    }

    @Test
    public void setCenter() throws Exception {
        Assert.assertEquals(center,sleeper.getCenter());
    }

}