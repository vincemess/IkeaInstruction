package sample.Utility;

import sample.model.InventaryBinary;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincenzo on 04/07/17.
 */
public class PagePDF {
    private List<String> paths = new ArrayList<>();
    private String text;
    private InventaryBinary inventaryBinary;
    private int NumberOfPiece;
    private String type;



    public int getNumberOfPiece() {
        return NumberOfPiece;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setNumberOfPiece(int numberOfPiece) {
        NumberOfPiece = numberOfPiece;
    }

    public InventaryBinary getInventaryBinary() {
        return inventaryBinary;
    }

    public void setInventaryBinary(InventaryBinary inventaryBinary) {
        this.inventaryBinary = inventaryBinary;
    }

    public List<String> getPaths() {
        return paths;
    }

    public void setPaths(List<String> paths) {
        this.paths = paths;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
