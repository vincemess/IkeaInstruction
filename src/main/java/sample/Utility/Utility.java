package sample.Utility;


import sample.model.Point;

/**
 * Created by vincenzo on 19/06/17.
 */
public class Utility {
    public static double distanceTwoPoint(Point a, Point b) {
        return Math.sqrt(Math.pow((a.getX() - b.getX()), 2) + Math.pow((a.getY() - b.getY()), 2));
    }
    public static double mean(double[] m) {
        double sum = 0;
        for (int i = 0; i < m.length; i++) {
            sum += m[i];
        }
        return sum / m.length;
    }
}
