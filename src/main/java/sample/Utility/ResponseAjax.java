package sample.Utility;

/**
 * Created by vincenzo on 01/07/17.
 */
public class ResponseAjax {
    String  response;

    public ResponseAjax(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
