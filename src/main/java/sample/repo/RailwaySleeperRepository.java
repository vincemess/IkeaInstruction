package sample.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sample.model.RailwaySleeper;
/**
 * Created by vincenzo on 03/07/17.
 */
@Repository
public interface RailwaySleeperRepository extends CrudRepository<RailwaySleeper, Long> {
}
