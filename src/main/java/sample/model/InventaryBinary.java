package sample.model;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by vincenzo on 16/06/17.
 */
@Entity
public class InventaryBinary {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String nameBinary;

    private int[] gridsNumber;

    private int numberSupportA;
    private int numberSupportB;
    private int numberSupportL;

    public static final int  NUMBEROFSUPPORTBNECESSARY = 2;
    public static final int  NUMBEROFSUPPORTLNECESSARY = 1;

    public InventaryBinary() {

    }

    public int getNumberSupportB() {
        return numberSupportB;
    }

    public void setNumberSupportB(int numberSupportB) {
        this.numberSupportB = numberSupportB;
    }

    public int getNumberSupportL() {
        return numberSupportL;
    }

    public void setNumberSupportL(int numberSupportL) {
        this.numberSupportL = numberSupportL;
    }

    public static int getNUMBEROFSUPPORTBNECESSARY() {
        return NUMBEROFSUPPORTBNECESSARY;
    }

    public static int getNUMBEROFSUPPORTLNECESSARY() {
        return NUMBEROFSUPPORTLNECESSARY;
    }


    public InventaryBinary(String nameBinary, int[] gridsNumber, int numberSupportA) {
        this.nameBinary = nameBinary;
        this.gridsNumber = gridsNumber;
        this.numberSupportA = numberSupportA;
        this.numberSupportB = numberSupportA * NUMBEROFSUPPORTBNECESSARY;
        this.numberSupportL = numberSupportA * NUMBEROFSUPPORTLNECESSARY;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNameBinary() {
        return nameBinary;
    }

    public void setNameBinary(String nameBinary) {
        this.nameBinary = nameBinary;
    }

    public int[] getGridsNumber() {
        return gridsNumber;
    }

    public void setGridsNumber(int[] gridsNumber) {
        this.gridsNumber = gridsNumber;
    }

    public int getNumberSupportA() {
        return numberSupportA;
    }

    public void setNumberSupportA(int numberSupportA) {
        this.numberSupportA = numberSupportA;
    }
}
