package sample.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vincenzo on 14/06/17.
 */
@Entity
public class GridPolyline {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    @OneToMany(cascade=CascadeType.ALL)
    private List<Point> points = new ArrayList<>();
    @OneToOne(cascade=CascadeType.ALL)
    private Point center = new Point();
    @OneToOne(cascade=CascadeType.ALL)
    private Point max = new Point();
    @OneToOne(cascade=CascadeType.ALL)
    private Point min = new Point();
    @OneToMany(cascade=CascadeType.ALL)
    private List<Point> objectCoordinates = new ArrayList<>();
    String path = "";
    @OneToOne(cascade=CascadeType.ALL)
    Point translationGridFromCenter;
    private int number;


    public GridPolyline() {

    }
    public GridPolyline(GridPolyline grid) {
        this.name = new String(grid.getName());
        for (Point point : grid.getPoints()) {
            this.points.add(new Point(point));
        }
        this.center =  new Point(grid.getCenter());
        this.max =  new Point(grid.getMax());
        this.min =  new Point(grid.getMin());
        this.number = grid.getNumber();
        this.objectCoordinates = calculateObjectCoord(this.center,this.points);
    }

    public GridPolyline(String name, List<Point> points,Point center,Point max, Point min) {
        this.name = name;
        this.points = points;
        this.center = center;
        this.max = max;
        this.min = min;
        this.objectCoordinates = calculateObjectCoord(this.center,this.points);
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Point getMax() {
        return max;
    }

    public void setMax(Point max) {
        this.max = max;
    }

    public Point getMin() {
        return min;
    }

    public void setMin(Point min) {
        this.min = min;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }



    public List<Point> getObjectCoordinates() {
        return objectCoordinates;
    }

    public void setObjectCoordinates(List<Point> objectCoordinates) {
        this.objectCoordinates = objectCoordinates;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Point getTranslationGridFromCenter() {
        return translationGridFromCenter;
    }

    public void setTranslationGridFromCenter(Point translationGridFromCenter) {
        this.translationGridFromCenter = translationGridFromCenter;
    }

    private List<Point> calculateObjectCoord(Point center, List<Point> points) {
        List<Point> objectCoordinatesXY = new ArrayList<>();
        for (int i = 0; i < points.size(); i++) {
            double pointObjectXY[] = new double[2];
            pointObjectXY[0] = points.get(i).getX() - center.getX();
            pointObjectXY[1] = points.get(i).getY() - center.getY();
            objectCoordinatesXY.add(new Point(pointObjectXY[0],pointObjectXY[1]));
        }
        return objectCoordinatesXY;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GridPolyline that = (GridPolyline) o;

        if (number != that.number) return false;
        if (points != null ? !points.equals(that.points) : that.points != null) return false;
        if (center != null ? !center.equals(that.center) : that.center != null) return false;
        if (max != null ? !max.equals(that.max) : that.max != null) return false;
        if (min != null ? !min.equals(that.min) : that.min != null) return false;
        return objectCoordinates != null ? objectCoordinates.equals(that.objectCoordinates) : that.objectCoordinates == null;
    }

    @Override
    public int hashCode() {
        int result = points != null ? points.hashCode() : 0;
        result = 31 * result + (center != null ? center.hashCode() : 0);
        result = 31 * result + (max != null ? max.hashCode() : 0);
        result = 31 * result + (min != null ? min.hashCode() : 0);
        result = 31 * result + (objectCoordinates != null ? objectCoordinates.hashCode() : 0);
        result = 31 * result + number;
        return result;
    }
}
