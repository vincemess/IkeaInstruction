package sample.model;


import javax.persistence.*;
import java.util.*;

/**
 * Created by vincenzo on 16/06/17.
 */
@Entity
public class BinaryTrain {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String camera;

    @OneToOne(cascade=CascadeType.ALL)
    Point translationFromOtherBinary;

    String name;
    @OneToOne(cascade=CascadeType.ALL)
    Rail rail;
    @ManyToMany(cascade=CascadeType.ALL)
    @OrderColumn
    GridPolyline[] grids;
    @OneToOne
    RailwaySleeper sleeper;
    @OneToMany(cascade=CascadeType.ALL)
    @OrderColumn
    SupportA[] supportAList;
    @OneToMany(cascade=CascadeType.ALL)
    @OrderColumn
    SupportB[] supportBList;
    @OneToMany(cascade=CascadeType.ALL)
    @OrderColumn
    SupportL[] supportLList;
    @OneToOne(cascade=CascadeType.ALL)
    Point center;
    @OneToOne(cascade=CascadeType.ALL)
    InventaryBinary inventaryBinary;

    double coeff_angular;


    private static double distanceBetweenSleepers = 0.6;


    public BinaryTrain(String name, Rail r, GridPolyline[] grids, RailwaySleeper sleeper, SupportA[] supportAList,SupportB[] supportBList,SupportL[] supportLList) {
        this.name = name;
        this.camera = name + "_camera";
        this.rail = r;
        this.grids = grids;
        this.sleeper = sleeper;
        this.center = this.grids[1].getCenter();
        this.supportAList = supportAList;
        this.supportBList = supportBList;
        this.supportLList = supportLList;
        calculateDistanceSleppersFromCenter(this.sleeper, this.center);
        this.rail.translationRailFromCenter = calculateDistanceRailFromCenter(this.grids, this.rail.angle);
        calculateDistanceGridFromCenter(this.grids, this.center);
        calculateDistanceSupportAFromCenter(this.supportAList,this.center);
        calculateDistanceSupportBFromCenter(this.supportBList,this.center);
        calculateDistanceSupportLFromCenter(this.supportLList,this.center);
        this.sleeper.translationBeetweenSleepers = calculateTranslationBetweenSleeper(this.coeff_angular);
        int[] numberGrids = new int[3];
        for (int i = 0; i < grids.length; i++) {
            numberGrids[i] = grids[i].getNumber();
        }
        this.inventaryBinary = new InventaryBinary(this.name,numberGrids,supportAList.length);
    }

    private Point calculateTranslationBetweenSleeper(double coeff_angular) {
        Point translation = new Point();
        translation.setX(distanceBetweenSleepers/ coeff_angular);
        translation.setY(distanceBetweenSleepers);
        return translation;
    }

    public BinaryTrain() {
    }


    private Point[] calculateDistanceSupportAFromCenter(SupportA[] supportAList, Point center) {
        Point[] translation = new Point[supportAList.length];
        for (int i = 0; i < supportAList.length; i+=2) {
            supportAList[i].translationSupportAFromCenter = new Point(rail.getTranslationRailFromCenter()[0].getX() ,rail.getTranslationRailFromCenter()[0].getX());
            supportAList[i+1].translationSupportAFromCenter = new Point(rail.getTranslationRailFromCenter()[1].getX() ,rail.getTranslationRailFromCenter()[1].getX());
        }
        return translation;
    }
    private Point[] calculateDistanceSupportLFromCenter(SupportL[] supportLList, Point center) {
        Point[] translation = new Point[supportAList.length];
        for (int i = 0; i < supportAList.length; i+=2) {
            supportLList[i].translationSupportLFromCenter = new Point(rail.getTranslationRailFromCenter()[0].getX() ,rail.getTranslationRailFromCenter()[0].getX());
            supportLList[i+1].translationSupportLFromCenter = new Point(rail.getTranslationRailFromCenter()[1].getX() ,rail.getTranslationRailFromCenter()[1].getX());
        }
        return translation;
    }
    private Point[] calculateDistanceSupportBFromCenter(SupportB[] supportBList, Point center) {
        Point[] translation = new Point[supportBList.length];
        for (int i = 0; i < supportBList.length; i+=2) {
            supportBList[i].translationSupportBFromCenter = new Point(rail.getTranslationRailFromCenter()[0].getX() ,rail.getTranslationRailFromCenter()[0].getX());
            supportBList[i+1].translationSupportBFromCenter = new Point(rail.getTranslationRailFromCenter()[1].getX() ,rail.getTranslationRailFromCenter()[1].getX());
        }
        return translation;
    }

    private void  calculateDistanceGridFromCenter(GridPolyline[] grids, Point center) {

        for (int i = 0; i < grids.length; i++) {
            grids[i].translationGridFromCenter = new Point(grids[i].getCenter().getX() - center.getX(),grids[i].getCenter().getY() - center.getY());
        }
    }

    private Point[] calculateDistanceRailFromCenter(GridPolyline[] grids, double[] angle) {
        Point[] translation = new Point[2];

        GridPolyline gridCenter = grids[1];
        List<Point> pointsTemp = new ArrayList<>(gridCenter.getPoints());
        Collections.sort(pointsTemp, new Comparator<Point>() {
            public int compare(Point p1, Point p2) {
                if(p1.getX()==p2.getX()) {// TODO controlla se eliminare punto
                    return 0;
                }else if(p1.getX()>=p2.getX())
                    return 1;
                else
                    return -1;
            }
        });
        List<Point> pointsOrdered = new ArrayList<>(pointsTemp);
        List<Point> pointsSX = new ArrayList<>(pointsOrdered.subList(0,2));
        List<Point> pointsDX = new ArrayList<>(pointsOrdered.subList(2,pointsOrdered.size()));

        double distanceRailGrid = 0.10;

        translation[0]= new Point((pointsSX.get(0).getX()+pointsSX.get(1).getX())/2 - distanceRailGrid - center.getX(),Math.abs(pointsSX.get(0).getY()+pointsSX.get(1).getY())/2 - center.getY());
        translation[1]= new Point((pointsDX.get(0).getX()+pointsDX.get(1).getX())/2 + distanceRailGrid - center.getX(),(pointsDX.get(0).getY()+pointsDX.get(1).getY())/2 - center.getY());

        double m_sx;
        if(pointsSX.get(0).getX()>pointsSX.get(1).getX())
             m_sx = (pointsSX.get(0).getY()-pointsSX.get(1).getY())/((pointsSX.get(0).getX()-pointsSX.get(1).getX()));
        else
            m_sx = (pointsSX.get(1).getY()-pointsSX.get(0).getY())/((pointsSX.get(1).getX()-pointsSX.get(0).getX()));

        double m_dx;
        if(pointsDX.get(0).getX()>pointsDX.get(1).getX())
                m_dx = (pointsDX.get(0).getY()-pointsDX.get(1).getY())/((pointsDX.get(0).getX()-pointsDX.get(1).getX()));
        else
                m_dx = (pointsDX.get(1).getY()-pointsDX.get(0).getY())/((pointsDX.get(1).getX()-pointsDX.get(0).getX()));

        angle[0] = Math.atan(1/m_sx);
        angle[1] = Math.atan(1/m_dx);

        this.coeff_angular = m_dx;

        System.out.println(Math.toDegrees(Math.atan(1/m_sx)));
        System.out.println(Math.toDegrees(Math.atan(1/m_dx)));

        return translation;
    }

    private void calculateDistanceSleppersFromCenter(RailwaySleeper sleeper, Point center) {
     sleeper.translationSleepersFromCenter = new Point(sleeper.getCenter().getX() - center.getX(),sleeper.getCenter().getY() - center.getY());

    }

    public SupportA[] getSupportAList() {
        return supportAList;
    }

    public void setSupportAList(SupportA[] supportAList) {
        this.supportAList = supportAList;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Point getTranslationFromOtherBinary() {
        return translationFromOtherBinary;
    }

    public void setTranslationFromOtherBinary(Point translationFromOtherBinary) {
        this.translationFromOtherBinary = translationFromOtherBinary;
    }

    public String getCamera() {
        return camera;
    }

    public void setCamera(String camera) {
        this.camera = camera;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Rail getRail() {
        return rail;
    }

    public void setRail(Rail rail) {
        this.rail = rail;
    }

    public GridPolyline[] getGrids() {
        return grids;
    }

    public void setGrids(GridPolyline[] grids) {
        this.grids = grids;
    }

    public RailwaySleeper getSleeper() {
        return sleeper;
    }

    public void setSleeper(RailwaySleeper sleepers) {
        this.sleeper = sleepers;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public double getDistanceBetweenSleepers() {
        return distanceBetweenSleepers;
    }

    public void setDistanceBetweenSleepers(double distanceBetweenSleepers) {
        this.distanceBetweenSleepers = distanceBetweenSleepers;
    }

    public SupportB[] getSupportBList() {
        return supportBList;
    }

    public void setSupportBList(SupportB[] supportBList) {
        this.supportBList = supportBList;
    }

    public InventaryBinary getInventaryBinary() {
        return inventaryBinary;
    }

    public void setInventaryBinary(InventaryBinary inventaryBinary) {
        this.inventaryBinary = inventaryBinary;
    }

    public double getCoeff_angular() {
        return coeff_angular;
    }

    public void setCoeff_angular(double coeff_angular) {
        this.coeff_angular = coeff_angular;
    }

    public SupportL[] getSupportLList() {
        return supportLList;
    }

    public void setSupportLList(SupportL[] supportLList) {
        this.supportLList = supportLList;
    }

}
