package sample.model;


import javax.persistence.*;

/**
 * Created by vincenzo on 07/06/17.
 */
@Entity
public class Line {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @OneToOne(cascade=CascadeType.ALL)
    private Point start;
    @OneToOne(cascade=CascadeType.ALL)
    private Point end;
    private double lenght;
    @OneToOne(cascade=CascadeType.ALL)
    Point center;
    double angle;

    public Line() {
    }

    public Line (Point p1, Point p2, double lenght){
        this.start = p1;
        this.end = p2;
        this.lenght = lenght;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getAngle() {
        return angle;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public double getLenght() {
        return lenght;
    }

    public void setLenght(double lenght) {
        this.lenght = lenght;
    }

    public Point getStart() {
        return start;
    }

    public void setStart(Point start) {
        this.start = start;
    }

    public Point getEnd() {
        return end;
    }

    public void setEnd(Point end) {
        this.end = end;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }
}