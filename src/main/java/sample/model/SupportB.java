package sample.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by vincenzo on 28/06/17.
 */
@Entity
public class SupportB extends Support {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  long id;
    @OneToOne(cascade=CascadeType.ALL)
    Point translationSupportBFromCenter;
    public static final int NECESSARYSCREWS = 2;


    public SupportB(String name, Point center) {
        this.name = name;
        this.center = center;
    }

    public SupportB() {
    }

    public SupportB(SupportB supportB) {
        this.center = new Point(supportB.getCenter());
        this.name = supportB.getName();
        this.path = supportB.getPath();
        this.translationSupportBFromCenter = new Point(supportB.getTranslationSupportBFromCenter());
    }

    public SupportB(String name,SupportB supportB) {
        this.center = new Point(supportB.getCenter());
        this.name = supportB.getName();
        this.path = supportB.getPath();
        this.translationSupportBFromCenter = new Point(supportB.getTranslationSupportBFromCenter());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public Point getTranslationSupportBFromCenter() {
        return translationSupportBFromCenter;
    }

    public void setTranslationSupportBFromCenter(Point translationSupportBFromCenter) {
        this.translationSupportBFromCenter = translationSupportBFromCenter;
    }
}
